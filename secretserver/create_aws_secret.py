
import http.client
import urllib
import json
import requests
import os
import argparse 
import logging, sys



def SetLogging(level):
    loglevelmap = {
        "critical": logging.CRITICAL,
        "error": logging.ERROR, 
        "warning": logging.WARNING,
        "debug": logging.DEBUG, 
        "info": logging.INFO,
        "notset": logging.NOTSET
    }    
    logging.basicConfig(stream=sys.stderr, level=loglevelmap[level.lower()] ) 
    return 

def GetEnvConf(env_key): 
    env_value = os.getenv(env_key)
    if env_value == None: 
        raise Exception("Unable to retrieve config from env: " + env_key)
    return env_value

def SetConfig(): 
    CONFIG = {}  

    parser = argparse.ArgumentParser(description='Create placeholder (stub) SecretServer secrets for AWS IAM Console Passwords.')
    parser.add_argument('--api_url', default='https://hasbro.secretservercloud.com/api/v1', help='Provide the full url of the SecretServer instance; ex: https://hasbro.secretservercloud.com/api/v1')
    parser.add_argument('--secret_name', default=None, help='Provide the name of the secret to be created')
    parser.add_argument('--username', default=None, help='Provide the username of the stored console login')
    parser.add_argument('--bearertoken', default=None, help='API authentication Bearer Token')
    parser.add_argument('--folderid', default=None, help='ID of folder to create the secret in.')
    parser.add_argument('--loglevel', default="error", help='Set Log output level')
    parser.add_argument('--secret_note', default=None, help='Set the Notes message for the secret')
    args = parser.parse_args()

    CONFIG["api_url"] = args.api_url
    CONFIG["secret_name"] = args.secret_name
    CONFIG["username"] = args.username
    CONFIG["folderid"] = args.folderid
    CONFIG["secret_note"] = args.secret_note
    if args.bearertoken != None: 
        CONFIG["token"] = args.bearertoken 
    else:
        CONFIG["token"] = GetEnvConf('SECRET_SERVER_BEARER_TOKEN')

    SetLogging(args.loglevel)

   
    return CONFIG; 

#REST call to create a generic a secret by ID
def CreateSecret(api, token,  body):
    headers = {'Authorization':'Bearer ' + token, 'content-type':'application/json'}
    resp = requests.post(api + '/secrets/' , headers=headers, json=body)    
    logging.debug("Creating secret: " + body["name"] )
    if resp.status_code not in (200, 304):
        raise Exception("Error creating Secret. %s %s" % (resp.status_code, resp))    
    return resp.json()


#REST call to create a generic a secret by ID
# ...  create the minimal skeleton for an AWS Console Password secret
def CreateDummyAwsConsoleSecret(api, token, folderId, secret_name='example', item_username  = "py_bryce_aws_console", item_note="This is an example Note"):
    template_items = [
        {"itemValue": item_username, "fieldName": "Username", "slug": "username", "fieldDescription": "The Amazon IAM Console username." }, 
        { "itemValue": item_note,      "fieldName": "Notes",      "slug": "notes",      "fieldDescription": "Any additional notes."} ]
    body = {
        "secretTemplateId": 6036, # AWS Console Password Template Id 
        "folderId": folderId,
        "items": template_items,
        "name": secret_name,
        "siteId": 1
    }
    return CreateSecret(api, token, body=body) 



#REST call to retrieve a secret by ID
def GetSecret(api, token, secretId):
    headers = {'Authorization':'Bearer ' + token, 'content-type':'application/json'}
    resp = requests.get(api + '/secrets/' + str(secretId), headers=headers)    
    
    if resp.status_code not in (200, 304):
        raise Exception("Error retrieving Secret. %s %s" % (resp.status_code, resp))    
    return resp.json()

#REST call method to update the secret on the server
def UpdateSecret(api, token, secret):        
    headers = {'Authorization':'Bearer ' + token, 'content-type':'application/json'}
    secretId = secret['id']
    resp = requests.put(api + '/secrets/' + str(secretId), json=secret, headers=headers)    
    
    if resp.status_code not in (200, 304):
        raise Exception("Error updating Secret. %s %s" % (resp.status_code, resp))    
    return resp.json()

#Retrieves the secret item by its "slug" value
def GetItemBySlug(secretItems, slug):
    for x in secret['items']:
        if x['slug'] == slug:
            return x
    raise Exception('Item not found for slug: %s' % slug)

#Updates the secret item on the secret with the updated secret item
def UpdateSecretItem(secret, updatedItem):
    secretItems = secret['items']
    for x in secretItems:
        if x['itemId'] == updatedItem['itemId']:
            x.update(updatedItem)
            return
    raise Exception('Secret item not found for item id: %s' % str(updatedItem['itemId']))


if __name__ == "__main__":
    CONFIG = SetConfig()
    logging.debug(CONFIG["secret_name"])
    logging.debug(CONFIG["username"])

    r = CreateDummyAwsConsoleSecret( api=CONFIG["api_url"], token=CONFIG["token"], 
                                    folderId=CONFIG["folderid"], secret_name=CONFIG["secret_name"], 
                                    item_username=CONFIG["username"],
                                    item_note=CONFIG["secret_note"]
                                    ) 
    logging.debug(r) 


    exit(0)

